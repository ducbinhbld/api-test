const express = require('express')
const app = express();
const Port = 3000;


app.get('/', (req, res) => {
    res.send('<h1>Hello Binh Be Bong</h1>')
})

app.get('/blog', (req, res) => {
    res.send('<h1>Hello Binh Be Bong "Blog1"</h1>')
})

app.listen(Port, () => {
    console.log('success with port ', Port)
})